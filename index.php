<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>xBootFormValidator - Validação de Formulário fácil via jQuery</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/flatly/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-1.12.3.min.js"></script>

	<!--
		Plugin
	-->
	<link rel="stylesheet" type="text/css" href="assets/css/xBootFormValidator.css">
	<script src="assets/js/xBootFormValidator.jquery.js"></script>
	<script src="assets/js/jquery.maskedinput.min.js"></script>
	<script>
		$(document).ready(function($) {
			$('form').xBootFormValidator();
		});
	</script>
	<!--
		Fim
	-->
</head>
<body>
	<div class="container-fluid">
		<div class="row">

			<div class="col-xs-12 col-md-6">

				<h1>xBootFormValidator</h1>
				<h2><a href="https://bitbucket.org/xandynnn/xbootformvalidator/get/5b7bef59e6ee.zip" download="5b7bef59e6ee.zip">Baixar / Download</a></h2>

				<h2>Chamada / Call:</h2>

				<code>
					$('form').xBootFormValidator();
				</code>

				<h4>Exemplo / Example:</h4>

				<code>
					&lt;link rel="stylesheet" type="text/css" href="assets/css/xBootFormValidator.css"&gt;
					<br />
					&lt;script src="assets/js/xBootFormValidator.jquery.js">&lt;/script&gt;
					<br />
					&lt;script src="assets/js/jquery.maskedinput.min.js">&lt;/script&gt;
					<br />
					&lt;script&gt;
					<br />
					&nbsp;&nbsp;$(document).ready(function($) {
					<br />
					&nbsp;&nbsp;&nbsp;&nbsp;$('form').xBootFormValidator();
					<br />
					&nbsp;&nbsp;});
					<br />
					&lt;/script&gt;
				</code>
&nbsp;
				<section>
					<h2>input</h2>
					<h4>data-required { boolean [ true ] [ false ] }</h4>
					<h4>data-regex { string  [ /([a,b])/ ], [ cpf ], [ cnpj ], [ email ], [ data ] }</h4>
					<h4>data-regexmsg { string [ Sua mensagem de erro ] }</h4>
					<h4>data-min { inteiro [ 1 ], [ 2 ], ... }</h4>
					<h4>data-max { inteiro [ 1 ], [ 2 ], ... }</h4>
					<h4>data-format { [999.999.999-99] }</h4>

					<h4>Exemplo / Example:</h4>

					<code>
					&lt;input type="text" data-required="true" data-regex="email" data-min="3" data-regexmsg="Seu e-mail é inválido" /&gt;
					<br />
					&lt;input type="text" data-regex="/\d{2}\/\d{2}\/\d{4}/g" data-format="99/99/9999" data-max="8" data-regexmsg="Data inválida" /&gt;
					</code>

				</section>

				<section>
					<h2>select</h2>
					<h4>data-required { boolean [ true ] } // Primeiro item não pode ficar selecionado</h4>
					<h4>Exemplo / Example:</h4>
					<code>
						&lt;select data-required="true" &gt;...&lt;/select&gt;
					</code>
				</section>

				<section>
					<h2>radio,<br />
					checkbox</h2>
					<h4>data-required { boolean [ true ] }</h4>
					<h4>Exemplo / Example:</h4>
					<code>
						&lt;input data-required="true" type="radio" name="optionsRadios" id="optionsRadios1" value="option1"&gt;
					</code>

				</section>

				<section>
					<h2>textarea</h2>
					<h4>data-required { boolean [ true ] }</h4>
					<h4>data-min { inteiro [ 1 ], [ 2 ], ... }</h4>
					<h4>data-max { inteiro [ 1 ], [ 2 ], ... }</h4>
					<h4>Exemplo / Example:</h4>
					<code>
						&lt;textarea class="form-control" data-required="true" data-max="8" data-min="3" rows="3" id="textArea"&gt;&lt;/textarea&gt;
					</code>

				</section>

				<br /><br /><br /><br />

			</div>

			<div class="col-xs-12 col-md-6">

				<h2>Formulário</h2>

				<form name="myform" class="form-horizontal" method="post" action="index.php">
				  <fieldset>
					<legend>Legend</legend>
					<div class="form-group">
					  <label for="inputEmail" class="col-lg-2 control-label">Email</label>
					  <div class="col-lg-10">
						<input type="text" class="form-control" data-regex="email" data-regexmsg="Seu email não é válido" id="inputEmail" placeholder="Email" data-required="true" data-min="3" data-max="15">
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputCpf" class="col-lg-2 control-label">Password</label>
					  <div class="col-lg-10">
						<input type="text" data-format="999.999.999-99" data-regex="cpf" class="form-control" id="inputCpf" placeholder="CPF" data-required="true">
					  </div>
					</div>
					<div class="form-group">
					  <label for="inputPassword" class="col-lg-2 control-label"></label>
					  <div class="col-lg-10">
						<div class="checkbox">
						  <label>
							<input name="chkitem" data-required="true" type="checkbox"> Checkbox
						  </label>
						</div>
					  </div>
					</div>
					<div class="form-group">
					  <label for="textArea" class="col-lg-2 control-label">Textarea</label>
					  <div class="col-lg-10">
						<textarea class="form-control" data-required="true" data-max="8" data-min="3" rows="3" id="textArea"></textarea>
						<span class="help-block">Texto de descrição.</span>
					  </div>
					</div>
					<div class="form-group">
					  <label class="col-lg-2 control-label">Radios</label>
					  <div class="col-lg-10">
						<div class="radio">
						  <label>
							<input data-required="true" type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
							Option one is this
						  </label>
						</div>
						<div class="radio">
						  <label>
							<input data-required="true" type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							Option two can be something else
						  </label>
						</div>
					  </div>
					</div>
					<div class="form-group">
					  <label for="select" class="col-lg-2 control-label">Selects</label>
					  <div class="col-lg-10">
						<select class="form-control" data-required="true" id="select">
						  <option>1</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
					  </div>
					</div>
					<div class="form-group">
					  <label for="select" class="col-lg-2 control-label">Selects</label>
					  <div class="col-lg-10">
						<select class="form-control" id="select">
						  <option>1</option>
						  <option>2</option>
						  <option>3</option>
						  <option>4</option>
						  <option>5</option>
						</select>
					  </div>
					</div>
					<div class="form-group">
					  <div class="col-lg-10 col-lg-offset-2">
						<button type="reset" class="btn btn-default">Cancel</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					  </div>
					</div>
				  </fieldset>
				</form>

			</div>

		</div>
	</div>
</body>
</html>