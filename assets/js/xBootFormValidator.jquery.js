// JavaScript jQuery Plugin xBootFormValidator - Alexandre Mattos
(function(){
	//Definicao do nome do plugin e chamada com opcoes
	jQuery.fn.xBootFormValidator = function(options){
		//Seta os valores default
		var defaults = {}
		var options = $.extend(defaults, options);
		//Inicializacao do plugin
		return this.each(function(){
			//Objeto
			var obj = jQuery(this);

			// Mascara
			obj.find('input, select, textarea').each(function(index, el) {
				var inputItem = $(this);
				if ( inputItem.attr("data-format") ){
					var format = inputItem.attr("data-format");
					inputItem.mask(format,{placeholder:" "});
				}
			});

			obj.find('textarea').each(function(index, el) {
				var txtA = $(this);
				var total = txtA.attr("data-max");

				txtA.attr("maxlength",total).parent().append("<div class='count'>0 / "+total+"</div>");

				txtA.on("keyup",function(){
					var atual = $(this).val().length;
					var count = obj.find("textarea").parent().find('.count');
					count.html(atual + ' / ' + total);
				});

			});

			//Envio do formulário
			obj.submit(function() {
				obj.find("p.msgerro").remove();
				obj.find('input, select, textarea').each(function(index, el) {

					var elemento = $(this);
					var tagname = elemento.prop("tagName");
					var name = elemento.attr("name");
					// INPUT
					if ( tagname == "INPUT" ){
						//RADIO CHECK
						if ( elemento.attr("type") == "radio" ){
							var elName = elemento.attr("name");
							var valorFinal = elemento.parents(".form-group").find("input[name=" + elName + "]:checked").val();
							if (!valorFinal){
								elemento.parents(".form-group").addClass('has-error');
							}else{
								elemento.parents(".form-group").removeClass('has-error');
							}
						}else if ( elemento.attr("type") == "checkbox" ){
							var elName = elemento.attr("name");
							var valorFinal = elemento.parents(".form-group").find("input[name=" + elName + "]:checked").val();
							if (!valorFinal){
								elemento.parents(".form-group").addClass('has-error');
							}else{
								elemento.parents(".form-group").removeClass('has-error');
							}
						}else if ( elemento.attr("type") == "file" ){

							//NÃO VALIDADO AINDA

						}else{

							// VALIDAÇÃO EXPRESSÃO REGULAR
							if ( elemento.attr("data-regex") ){

								var regEx = elemento.attr("data-regex");
								if ( regEx == "email" ){
									var rE = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
									var mensagem = "E-mail não é válido";
								}else if ( regEx == "data" ){
									var rE = /\d{2}\/\d{2}\/\d{4}/g;
									var mensagem = "data não é válida";
								}else if ( regEx == "cpf"){
									var rE = /^\d{3}\.\d{3}\.\d{3}\-\d{2}$/i;
									var mensagem = "CPF não é válido";
								}else if ( regEx == "cnpj"){
									var rE = /^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/i;
									var mensagem = "CNPJ não é válido";
								}else{
									var rE = new RegExp(regEx);
									var mensagem = elemento.attr("data-regexmsg");
								}

								if ( !rE.test(elemento.val()) ){
									elemento.parents(".form-group").addClass('has-error');
									if ( elemento.parent().prop("tagName") == "DIV" ){
										elemento.parent().append('<p class="msgerro">' + mensagem + '</p>');
									}
								}else{
									elemento.parents(".form-group").removeClass('has-error');
									elemento.parents(".form-group").find("p.msgerro").remove();
								}
							}else{

								// VALIDAÇÃO POR REQUIRED MIN OU MAX
								if ( elemento.attr("data-required") == "true" ){
									if ( elemento.val().length == 0 ){

										elemento.parents(".form-group").addClass('has-error');
										var campoNome = "";
										if ( elemento.parents(".form-group").find("label").html() != undefined ){
											campoNome = ' ' + elemento.parents(".form-group").find("label").html();
										}else if ( elemento.attr("placeholder") != undefined ) {
											campoNome = ' ' + elemento.attr("placeholder");
										}
										if ( elemento.parent().prop("tagName") == "DIV" ){
											elemento.parent().append('<p class="msgerro">O campo' + campoNome +' não pode ficar vazio.</p>');
										}

									}else{
										var min = elemento.attr("data-min");
										var max = elemento.attr("data-max");
										var elSize = elemento.val().length;
										if ( min ){
											if ( elSize < min ){
												elemento.parents(".form-group").addClass('has-error');
												if ( elemento.parent().prop("tagName") == "DIV" ){
													elemento.parent().append('<p class="msgerro">Mínimo de ' + min + ' caracteres.</p>');
												}
											}else{
												if ( max ){
													if ( elSize > max ){
														elemento.parents(".form-group").addClass('has-error');
														if ( elemento.parent().prop("tagName") == "DIV" ){
															elemento.parent().append('<p class="msgerro">Máximo de ' + max + ' caracteres.</p>');
														}
													}else{
														elemento.parents(".form-group").removeClass('has-error');
														elemento.parents(".form-group").find("p.msgerro").remove();
													}
												}else{
													elemento.parents(".form-group").removeClass('has-error');
													elemento.parents(".form-group").find("p.msgerro").remove();
												}
											}
										}else{
											elemento.parents(".form-group").removeClass('has-error');
											elemento.parents(".form-group").find("p.msgerro").remove();
										}
									}
								}
							}
						}
					// SELECT
					}else if( tagname == "SELECT" ){
						// Validação por required True nao precisa do size
						if ( elemento.attr("data-required") == "true" ){
							if ( elemento.find("option").eq(0).val() == elemento.val() ){
								elemento.parents(".form-group").addClass('has-error');
								if ( elemento.parent().prop("tagName") == "DIV" ){
									elemento.parent().append('<p class="msgerro">Selecione um item</p>');
								}
							}else{
								elemento.parents(".form-group").removeClass('has-error');
							}
						}
					// TEXTAREA
					}else if( tagname == "TEXTAREA"){
						// Validação por required
						if ( elemento.attr("data-required") == "true" ){
							if ( elemento.val().length == 0 ){
								elemento.parents(".form-group").addClass('has-error');
								if ( elemento.parent().prop("tagName") == "DIV" ){
									elemento.parent().append('<p class="msgerro">O campo é obrigatório.</p>');
								}
							}else{
								var min = elemento.attr("data-min");
								var max = elemento.attr("data-max");
								var elSize = elemento.val().length;
								if ( min ){
									if ( elSize <= min ){
										elemento.parents(".form-group").addClass('has-error');
										elemento.parent().append('<p class="msgerro">Mínimo de '+min+' caracteres .</p>');
									}else{
										if ( max ){
											if ( elSize > max ){
												elemento.parents(".form-group").addClass('has-error');
												elemento.parent().append('<p class="msgerro">Máximo de '+max+' caracteres.</p>');
											}else{
												elemento.parents(".form-group").removeClass('has-error');
											}
										}else{
											elemento.parents(".form-group").removeClass('has-error');
										}
									}
								}else{
									elemento.parents(".form-group").removeClass('has-error');
								}
							}
						}
					}
				});

				if ( !obj.find('.has-error').length == 0){
					return false;
				}
			});

		}); //Fim do processo
	}
})(jQuery);